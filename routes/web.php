<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'Site\SiteController@index')->name('home');


$this->group(['middleware' => ['auth'], 'namespace' => 'Admin','prefix'=>'admin'], function () {

    $this->get('club','ClubController@index')->name('admin.club');

    $this->get('/', 'AdminController@index')->name('admin.home');
});

Auth::routes();






